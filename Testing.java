import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Testing {

    public static void main(String[] args) {
        multipleAsyncCall();
    }

    public static void parallelCall() {

        ExecutorService executors = Executors.newFixedThreadPool(4);
        CompletableFuture.supplyAsync(new Add(10, 20), executors);
        CompletableFuture.supplyAsync(new Multiply(2, 5), executors);
        executors.shutdown();
        System.out.println("multiply should be called first, then add");
    }

    public static void waitAnotherAsyncCall() {
        // in order for this to happen, we need to use thenCompose

        long startTime = System.nanoTime();

        ExecutorService executors = Executors.newFixedThreadPool(4);
        CompletableFuture<Integer> multiplyResult = CompletableFuture.supplyAsync(new Multiply(2, 5), executors);
        CompletableFuture<Integer> subtractResult = multiplyResult.thenCompose(result -> {
            return CompletableFuture.supplyAsync(new Subtract(result, 10));
        });

        subtractResult.thenAccept((result) -> {
            System.out.println("After subtract, the result should be " + result);
        })
                .thenRun(() -> {
                    long stopTime = System.nanoTime();
                    System.out.println(((stopTime - startTime) * Math.pow(10, 9)) + " seconds");
                    executors.shutdown();
                });

        System.out.println("in this case, the running time should be 1.5 second");
        System.out.println("in this case, multiplication should run first, then subtract");
    }

    public static void waitForParalelCall() {
        // in order for this to happen, we need to use thenCombine
        long startTime = System.nanoTime();

        ExecutorService executors = Executors.newFixedThreadPool(4);
        CompletableFuture<Integer> multiplyResult = CompletableFuture.supplyAsync(new Multiply(2, 5), executors)
                .thenCombine(
                        CompletableFuture.supplyAsync(new Subtract(20, 10), executors),
                        (multResult, subtractResult) -> {
                            return multResult + subtractResult;
                        });

        multiplyResult.thenAccept((result) -> {
            System.out.println("After add, the result should be " + result);
            long stopTime = System.nanoTime();
            System.out.println(((stopTime - startTime) * Math.pow(10, 9)) + " seconds");
            executors.shutdown();
        });

        System.out.println("in this case, the running time should be 1 second");
        System.out.println("in this case, subtract should call first, then multiply");

    }

    public static void multipleAsyncCall() {
        long startTime = System.nanoTime();

        ExecutorService executors = Executors.newFixedThreadPool(4);
        CompletableFuture<Integer> addFuture = CompletableFuture.supplyAsync(new Add(10, 20), executors);
        CompletableFuture<Integer> multiplyFuture = CompletableFuture.supplyAsync(new Multiply(2, 5), executors);
        CompletableFuture<Integer> subtractFuture = CompletableFuture.supplyAsync(new Subtract(40, 20), executors);

        CompletableFuture.allOf(addFuture, multiplyFuture, subtractFuture)
                .thenAccept(iterator -> {
                    int a = addFuture.join();
                    int b = multiplyFuture.join();
                    int c = subtractFuture.join();

                    System.out.println((a + b + c) + " the results of adding it all");
                })
                .thenRun(() -> {

                    long stopTime = System.nanoTime();

                    System.out.println(((stopTime - startTime) * Math.pow(10, 9)) + " seconds");
                    executors.shutdown();
                });

        System.out.println("in this case, the running time should be 1.5 second");
        System.out.println("should call subtract, multiply, then add respectively");
    }
}