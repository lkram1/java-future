import java.util.function.Supplier;

public class Add implements Supplier<Integer> {

    private final int a;
    private final int b;

    public Add(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public Integer get() {
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int result = a + b;

        System.out.println("the addition result are " + result);

        return result;
    }

}
