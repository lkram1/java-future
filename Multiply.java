import java.util.function.Supplier;

class Multiply implements Supplier<Integer> {

    private final int a;
    private final int b;

    public Multiply(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public Integer get() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int result = a * b;
        System.out.println("the multiplication result are " + result);

        return result;
    }

}
