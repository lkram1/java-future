import java.util.function.Supplier;

public class Subtract implements Supplier<Integer> {

    private final int a;
    private final int b;

    public Subtract(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public Integer get() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int result = a - b;

        System.out.println("the subtraction result are " + result);

        return result;
    }

}
